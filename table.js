mdc.autoInit();

const suits = ["♠", "♡", "♢", "♣"];
const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];

const mapRanksToWords = { 2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine", 10: "Ten" };
const mapSuitsToWords = { "": "Mystery", "♠": "Spades", "♡": "Hearts", "♢": "Diamonds", "♣": "Clubs" };

function getDeck() {
  const deck = [];
  for (const suit of suits) {
      for (const rank of ranks) {
          deck.push({ suit, rank });
      }
  }
  return deck;
}

function getRandomCard(deck) {
  const randomIndex = Math.floor(Math.random() * deck.length);
  const randomCard = deck[randomIndex];
  deck.splice(randomIndex, 1);
  return randomCard;
}

function rankToWord(rank) {
    if (typeof rank === 'number') {
        return mapRanksToWords[rank];
    }
    return rank;
}

function suitToWord(suit) {
    return mapSuitsToWords[suit];
}

function rankToValue(rank) {
    if (typeof rank === 'number') {
        return rank.toString();
    } else if (rank === "Ace") {
        return "11/1";
    } else if (rank === "Knave" || rank === "Queen" || rank === "King") {
        return "10";
    } else {
        return "?";
    }
}

function dealToDisplay(card) {
    const cardString = `${rankToWord(card.rank)} of ${suitToWord(card.suit)}`;
    const blackjackValue = rankToValue(card.rank);
    console.log(`Your card is ${cardString}. Blackjack value: ${blackjackValue}. Did you bust?`);
}

const hitButton = document.getElementById("hit-button");
hitButton.addEventListener("click", () => {
    const deck = getDeck();
    const randomCard = getRandomCard(deck);
    dealToDisplay(randomCard);
});

let bankroll = 2022;

function getBankroll() {
    return bankroll;
}

function setBankroll(newBalance) {
    bankroll = newBalance;
}

function timeToBet() {
    document.getElementById("playersActions").style.display = "none";
    document.getElementById("betting").style.display = "block";
}

function makeWager() {
    const wagerAmount = document.getElementById("users-wager").value;
    console.log(wagerAmount);
    timeToPlay();
}

function timeToPlay() {
    document.getElementById("playersActions").style.display = "block";
    document.getElementById("betting").style.display = "none";
}

const betButton = document.getElementById("bet-button");
betButton.addEventListener("click", makeWager);