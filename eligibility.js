mdc.autoInit();

document.addEventListener("DOMContentLoaded", function () {
    const signUpButton = document.getElementById("sign-up-btn");

    signUpButton.addEventListener("click", function (event) {
        event.preventDefault(); // Prevent the default form submission

        // Capture and log each text field's label and value
        const textFields = document.querySelectorAll(".mdc-text-field__input");
        textFields.forEach((textField) => {
            const label = textField.previousElementSibling.textContent;
            const value = textField.value;
            console.log(`${label.trim()}: ${value}`);
        });

        // Check the status of checkboxes and log appropriate statements
        const legalCheckbox = document.getElementById("legal-checkbox");
        const termsCheckbox = document.getElementById("terms-checkbox");
        const legalChecked = legalCheckbox.checked;
        const termsChecked = termsCheckbox.checked;

        if (legalChecked && termsChecked) {
            console.log("The user has checked the legal checkbox");
            console.log("The user has checked the terms checkbox");
        } else if (legalChecked) {
            console.log("The user has checked the legal checkbox");
            console.log("The user has not checked the terms checkbox");
        } else if (termsChecked) {
            console.log("The user has not checked the legal checkbox");
            console.log("The user has checked the terms checkbox");
        } else {
            console.log("The user has not checked the legal checkbox");
            console.log("The user has not checked the terms checkbox");
        }

        // Validate user eligibility
        const passwordField = document.getElementById("password");
        const confirmPasswordField = document.getElementById("confirm-password");
        const ageField = document.getElementById("age");

        const password = passwordField.value;
        const confirmPassword = confirmPasswordField.value;
        const age = parseInt(ageField.value, 10);

        if (
            password === confirmPassword &&
            age >= 13 &&
            legalChecked &&
            termsChecked &&
            textFields.every((textField) => textField.value.trim() !== "")
        ) {
            console.log("The user is eligible");

            // Compare age and birthdate
            const birthDateField = document.getElementById("birth-date");
            const birthDateValue = new Date(birthDateField.value);
            const currentYear = new Date().getFullYear();
            const userAge = currentYear - birthDateValue.getFullYear();

            if (userAge === age) {
                console.log("The user can figure out their age");
            } else {
                console.log("The user is not likely to be good at math");
            }
        } else {
            console.log("The user is ineligible");
        }
    });
});